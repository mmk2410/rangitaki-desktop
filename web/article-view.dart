@HtmlImport('article_view.html')
library mmk2410_rangitaki_app.article_view;

import 'package:polymer/polymer.dart';
import 'package:web_components/web_components.dart' show HtmlImport;

@PolymerRegister('article-view')
class ArticleView extends PolymerElement {

  ArticleView.created() : super.created();

  @property
  String title;
  String author;
  String date;
  String text;
  String tags;
}
