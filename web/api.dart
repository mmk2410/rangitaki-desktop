import 'dart:async';
import 'dart:html';
import 'dart:convert';

import 'package:polymer_elements/paper_item.dart';

import 'article-view.dart';

/**
 * Easily connects with the Rangitaki API and does also a part of the frontend.
 */
class RangitakiConnect {

  String baseurl;
  String _username;
  String _password;
  String _auth;

  /**
   * Initializer for RangitaiConnect.
   * @value baseurl Root URL of your Rangitaki installation.
   * @value username Username for authentication.
   * @value password Password for authentication.
   */
  RangitakiConnect(this.baseurl, this._username, this._password) {
    _auth = BASE64.encode(UTF8.encode("$_username:$_password"));
  }

  /**
   * Fetch all blogs from the server and list them in the #list element.
   */
  Future addBlogs() async {
    var data = await _getBlogs();
    data = JSON.decode(data);
    for (String blog in data) {
      blog = blog.substring(0, blog.length - 3);
      PaperItem item = new PaperItem();
      item.text = blog;
      item.id = blog;
      querySelector("#main-list").children.add(item);
    }
  }

  /**
   * Fetch all articles of a blog from the server and list the in the #list
   * element.
   * @value blog Blog for which the articles shoul be fetched.
   */
  Future addArticles(String blog) async {
    var data = await _getArticles(blog);

    data = JSON.decode(data);
    for (String article in data) {
      article = article.substring(0, article.length - 3);
      PaperItem item = new PaperItem();
      item.text = article;
      item.id = article;
      querySelector("#main-list").children.add(item);
    }
  }

  Future showArticle(String blog, String post, ArticleView articleView) async {
    var data = await _getArticle(blog, post);

    print(data);

    data = JSON.decode(data);
    List tags = data['tags'];
    String tag = tags.join(", ");

    print("Tags: $tag");

    print("ArticleView: $articleView");

    articleView
      ..title = data['title']
      ..author = data['author']
      ..date = data['date']
      ..tags = tag
      ..text = data['text'];
  }

  /**
   * Internal method for fetching blogs from the server.
   */
  Future _getBlogs() async {
    String _url = "$baseurl/rcc/api/list/";
    return (await
      HttpRequest.request(
        _url, withCredentials: true,
        requestHeaders: {'authorization' : 'Basic $_auth'}
      )).responseText;
  }

  /**
   * Internal method for fetching articles of blog from the server.
   */
  Future _getArticles(String blog) async {
    String _url = "$baseurl/rcc/api/list/?blog=$blog";

    return (await
      HttpRequest.request(
        _url, withCredentials: true,
        requestHeaders: {'authorization' : 'Basic $_auth'}
      )).responseText;
  }

  Future _getArticle(blog, post) async {
    print("Blog: $blog");
    print("Post: $post");
    String _url = "$baseurl/rcc/api/post/?blog=$blog&post=$post.md";
    print("URL: $_url");
    return (await
      HttpRequest.request(_url, withCredentials: true,
      requestHeaders: {'authorization' : 'Basic $_auth'}
    )).responseText;
  }

}
