/**
 * @author Marcel Kapfer (mmk2410) <marcelmichaelkapfer@yahoo.co.nz>
 * Library for easily working with the Rangitaki API in your browswer.
 */
library mmk2410_rangitaki_app.index;

import 'dart:html';
import 'dart:async';

import 'package:polymer/polymer.dart';

import 'api.dart';
import 'article-view.dart';

export 'article-view.dart';
export 'package:polymer/init.dart';

final String username = "";
final String pwd = "";
final String baseurl = "";

RangitakiConnect rcc;
Element itemList;
ArticleView articleView;
String currentBlog;

main() async {
  await initPolymer();

  print("Starting...");

  rcc = new RangitakiConnect(baseurl, username, pwd);

  await rcc.addBlogs();

  itemList = querySelector("#main-list");

  for (Element item in itemList.children) {
    item.onClick.listen(blogSelected);
  }

  articleView = querySelector("articlev-iew");
}

Future blogSelected(Event e) async {
  Element element = e.target;
  itemList.children.clear();
  currentBlog = element.id;
  await rcc.addArticles(element.id);

  for (Element item in itemList.children) {
    item.onClick.listen(articleSelected);
  }
}

Future articleSelected(Event e) async {
  Element element = e.target;
  await rcc.showArticle(currentBlog, element.id, articleView);
  itemList.hidden = true;
  articleView.hidden = false;
}
