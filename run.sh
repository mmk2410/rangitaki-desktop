#!/bin/bash

while true; do
  echo -n " (BCLI) > "
  read -r command

  case $command in
    build | b ) pub build & ;;
    run | r )
      killall -q electron
      electron build/web/ &
    ;;
    bar | br )
      killall -q electron
      pub build
      electron build/web/ &
    ;;
    get | g) pub get ;;
    close | c) killall -q electron ;;
    test | t) pub run test;;
    lint | l)
      jshint web/main.js
      jshint web/renderer.js
      dartanalyzer web/
      ;;
    exit | quit | q | x | e)
      exit ;;
    about ) echo "mmk2410 - Build CLI script -  MIT License" ;;
    help | h | ? )
      echo """
run, r                Close running instances and run Electron in background
build, b              Build app in background
bar, br               Short for build and run
test, t               Run tests.
close, c              Exit running Electron instances
exit, quit, q, x, e   Exit the script
about                 About text
get, g                Get updates.
help, h, ?            Prints this help
      """
    ;;
    * ) echo "Command $command not found." ;;
  esac
done
