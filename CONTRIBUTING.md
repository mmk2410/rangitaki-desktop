# Contributing

If you want to contribute have a look at the project board at [phab.mmk2410.org](https://phab.mmk2410.org/project/view/19/) to see what you should do.

## Workflow

While there is no stable release this project will follow the feature workflow. As soon as there is a stable release I will switch to the Gitflow workflow.

If your unfamiliar with these workflows I recommend reading [this article](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow/).

## Contact

If you want to ask me some questions, feel free to contact me at (marcelmichaelkapfer@yahoo.co.nz)[mailto:marcelmichaelkapfer@yahoo.co.nz].

## Honor

Please add yourself to the AUTHORS file.
